const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const errorController = require('./controllers/error404');




const sequelize = require('./util/database');
const Product = require('./models/product');
const User = require('./models/user');

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.getError404);

Product.belongsTo(User, { constraints: true, onDelete: 'CASCADE' });
User.hasMany(Product);
//  in questa manierà definirà anche le relazione, solo che se le abbiamo create  dobbiamo usare il sync({force:true}) che si può usare solo in dev 
sequelize
.sync()
.then(result => {
    User.findByPk(1);
    // console.log(result);
})
.then( user => {
    if (!user) {
        User.create({name: 'Ale', email: 'sandro.dilillo@gmail.com'})
    }
    return Promise.resolve(user); ///potrebbe essere anche return user solamente 
})
.then(user => {
    console.log(user);
    app.listen(3000);
})
.catch(err => {
    console.log(err);
});

// app.listen(3000);
