const fs = require('fs');

const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;
    if (url === '/') {

        res.setHeader('Content-Type', 'text/html'); /*qui sto dicendo che abbiamo un content di tipo html ma non sto dicendo il contenuto ma solo il tipo */
        res.write('<html>');
        res.write('<head><title>Enter message</title></head>');
        res.write('<body><form action="/message" method="POST"><input type="text" name="message"><button type="submit"> Send </button></input></form> </head>');
        res.write('</html>');
        return res.end(); 

   }
    if (url === '/message' && method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
        console.log(chunk);
        body.push(chunk);
    }); /* si usa molto in node, creando il server lo fa implicitamente  ascolta l'evento*/
    return req.on('end', () => {
    const parsedBody = Buffer.concat(body).toString();
 //   console.log(parsedBody);  
    const message = parsedBody.split('=')[1];
     fs.writeFile('message.txt', message, (err) => {

         res.statusCode = 302;
         res.setHeader('Location', '/');
         return res.end();
     });
 });

}
 res.setHeader('Content-Type', 'text/html'); /*qui sto dicendo che abbiamo un content di tipo html ma non sto dicendo il contenuto ma solo il tipo */
 res.write('<html>');
 res.write('<head><title>My first page</title></head>');
 res.write('<body><h1>Hello from my node server!</h1></head>');
 res.write('</html>');
 res.end(); /*non possiamo scrivere altro, la risposta finisce qui*/
};


module.exports = requestHandler;
// module.expots = {
//     handler: requestHandler,
//     someText: 'Some hard coded text',
// }
